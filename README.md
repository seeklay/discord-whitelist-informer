# Discord Whitelist Informer

### Functions
- [x] Send discord notify when new player was added to whitelist
- [x] Config.yaml with all settings (chanel, token, message content)
- [x] Event sources: RCON and Server Console, cmd: /whitelist add {username}

### Build req
- Maven
- Javacord
- Spigot API 1.8.8+
- Java 8+

### Build a jar
`mvn compile assembly:single`
