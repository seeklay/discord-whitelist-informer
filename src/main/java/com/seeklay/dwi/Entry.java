package com.seeklay.dwi;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.UserStatus;

import java.awt.Color;
import java.io.File;
import java.util.Optional;


public class Entry extends JavaPlugin implements Listener {
    
    DiscordApi api;
    Channel channel;    

    String DISCORD_TOKEN;
    String CHANNEL_ID;
    String MESSAGE_TITLE;
    String MESSAGE_DESCRIPTION;
    String MESSAGE_COLOR;
    String MESSAGE_IMG_URL;

    private static final String ERR = "\n---------------------------ERROR---------------------------\n";

    public void onDisable() {
        if (api != null) {
            api.updateStatus(UserStatus.OFFLINE);
            api.disconnect();
        }
    }
    public void onEnable() {
        getLogger().info("People stuck at windows...");

        // Config
        File configFile = new File(getDataFolder() + File.separator + "config.yml"); //This will get the config file
        if (!configFile.exists()) {
            saveDefaultConfig();
            getLogger().severe(ERR + "First startup! Specify DISCORD_TOKEN and CHANNEL_ID in plugin's config.yml!\n" + ERR);
            getServer().getPluginManager().disablePlugin(this);
            return;

        } else {
            getConfig().addDefault("enabled", null);
            getConfig().addDefault("DISCORD_TOKEN", null);
            getConfig().addDefault("CHANNEL_ID", null);
            getConfig().addDefault("MESSAGE_TITLE", null);
            getConfig().addDefault("MESSAGE_DESCRIPTION", null);
            getConfig().addDefault("MESSAGE_COLOR", null);
            getConfig().addDefault("MESSAGE_IMG_URL", null);

            FileConfiguration conf = getConfig();

            final boolean ENABLED = conf.getBoolean("enabled");
            DISCORD_TOKEN = conf.getString("DISCORD_TOKEN");
            CHANNEL_ID = conf.getString("CHANNEL_ID");

            MESSAGE_TITLE = conf.getString("MESSAGE_TITLE");
            MESSAGE_DESCRIPTION = conf.getString("MESSAGE_DESCRIPTION");
            MESSAGE_COLOR = conf.getString("MESSAGE_COLOR");
            MESSAGE_IMG_URL = conf.getString("MESSAGE_IMG_URL");

            if (!ENABLED) {
                getLogger().warning("Plugin was disabled in configuration!");
                getServer().getPluginManager().disablePlugin(this);
                return;
            }

            if (DISCORD_TOKEN == null | CHANNEL_ID == null) {
                getLogger().severe(ERR + "Invalid configuration! DISCORD_TOKEN and CHANNEL_ID must be present in config.yml!\n" + ERR);
                getServer().getPluginManager().disablePlugin(this);
                return;
            }

            if (DISCORD_TOKEN.isBlank() | CHANNEL_ID.isBlank()) {
                getLogger().severe(ERR + "Invalid configuration! DISCORD_TOKEN and CHANNEL_ID cannot be empty!\n" + ERR);
                getServer().getPluginManager().disablePlugin(this);
                return;
            }

            if (MESSAGE_TITLE == null) {
                getLogger().severe(ERR + "Invalid configuration! MESSAGE_TITLE must be present in config.yml!\n" + ERR);
                getServer().getPluginManager().disablePlugin(this);
                return;
            }
        }

        // Make connection
        try {
            api = new DiscordApiBuilder().setToken(DISCORD_TOKEN).login().join();
        } catch (Exception e) {
            String d = "\n---------------------------DISCORD---ERROR---------------------------\n";
            getLogger().severe(d + "Failed to connect to the discord due to an error: \n" + e + d);
            return;
        }

        getLogger().info("Logged in as " + api.getYourself().getDiscriminatedName());
        //api.updateActivity(ActivityType.WATCHING, "over the world");

        // Get channel
        Optional<Channel> pchannel = api.getChannelById(CHANNEL_ID);
        if (pchannel.isEmpty()) {
            String d = "\n---------------------------DISCORD---ERROR---------------------------\n";
            getLogger().severe(d + "Failed to find discord channel with id: \n" + CHANNEL_ID + d);
            api.updateStatus(UserStatus.OFFLINE);
            api.disconnect();
            return;
        }
        channel = pchannel.get();
        getLogger().info("Information channel - " + channel.asTextChannel().get().toString());

        // Registering commands handlers
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommandPreProcess(org.bukkit.event.server.ServerCommandEvent event){
        whitelistInformer(event.getCommand());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommandPreProcess(org.bukkit.event.server.RemoteServerCommandEvent event){
        whitelistInformer(event.getCommand());
    }

    public void whitelistInformer(String command) {
        String[] scommand = command.split(" ");
        if (scommand.length < 3) { return; }

        if (scommand[0].equalsIgnoreCase("whitelist") && scommand[1].equalsIgnoreCase("add")) {
            String TITLE = MESSAGE_TITLE.replaceAll("%username%", scommand[2]);

            EmbedBuilder embed = new EmbedBuilder();
            embed.setTitle(TITLE);


            if (MESSAGE_COLOR != null) {
                embed.setColor(Color.decode(MESSAGE_COLOR));
            }
            if (MESSAGE_DESCRIPTION != null) {
                embed.setDescription(MESSAGE_DESCRIPTION);
            }
            if (MESSAGE_IMG_URL != null) {
                embed.setThumbnail(MESSAGE_IMG_URL);
            }

            channel.asTextChannel().get().sendMessage(embed);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("psaw")) {
            sender.sendMessage("Weh the bloodclat do you, siddung");
            return true;
        }
        return false;
    }
}
